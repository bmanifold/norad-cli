class CreateResults < ActiveRecord::Migration[5.0]
  def change
    create_table :results do |t|
      t.string :assessment_id
      t.string :status
      t.text :output
      t.string :title
      t.string :description
      t.string :nid
      t.string :sir

      t.timestamps
    end
  end
end
