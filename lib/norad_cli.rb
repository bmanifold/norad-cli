# frozen_string_literal: true

require 'norad_cli/version'
require 'norad_cli/cli/main'

module NoradCli
  def self.ssh_key_path
    "#{__dir__}/norad_cli/templates/spec/support/ssh_key"
  end
end
